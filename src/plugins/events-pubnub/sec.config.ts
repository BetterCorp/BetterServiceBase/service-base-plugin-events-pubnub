import { randomUUID } from 'crypto';

export interface IPluginConfig {
  subscribeKey: string;
  publishKey?: string;
  cipherKey?: string;
  authKey?: string;
  logVerbosity?: boolean;
  uuid: string;
  ssl?: boolean;
  origin?: string;
  presenceTimeout?: number;
  heartbeatInterval?: number;
  restore?: boolean;
  keepAlive?: boolean;
  keepAliveSettings?: any;
  suppressLeaveEvents?: boolean;
  requestMessageCountThreshold?: number;
  autoNetworkDetection?: boolean;
  listenToBrowserNetworkEvents?: boolean;
  useRandomIVs?: boolean;
}

export default (): IPluginConfig => {
  return {
    subscribeKey: "",
    uuid: randomUUID()
  };
};