import * as pubnub from 'pubnub';
import { CEvents } from '@bettercorp/service-base/lib/interfaces/events';
import { Tools } from '@bettercorp/tools/lib/Tools';
//import { v4 as UUID } from 'uuid';
import * as EVENT_EMITTER from 'events';
import * as OS from 'os';
import { IPluginConfig } from './sec.config';
import { Readable } from 'stream';

/*interface internalEvent<T> {
  data: T;
  resultSuccess: Boolean;
  id: string;
}
interface transEAREvent<T> {
  data: T;
  topic: string;
  plugin: string;
  id: string;
}*/

export class Events extends CEvents<IPluginConfig> {
  //private sendQu: any;
  private pubnubConnection: any;
  private myResponseName!: string;
  private readonly pubnubConnectionEmitChannelKey = "eq";
  private readonly pubnubConnectionEmitTTL = (60 * 60 * 60 * 6) * 1000; // 6h
  //private readonly pubnubConnectionEARChannelKey = "ar";
  //private readonly pubnubConnectionEARTTL = (60 * 60 * 60) * 1000; // 60 minutes
  //private readonly pubnubConnectionEARChannelKeyMine = "kr";
  private builtInEvents: any;

  init(): Promise<void> {
    const self = this;
    return new Promise(async (resolve, reject) => {
      try {
        //self.sendQu = new (require('@goodware/task-queue'))({ size: 1 });
        await self._connectToPubnub();
        resolve();
      } catch (exce) {
        self.log.fatal(exce);
        reject(exce);
      }
    });
  }
  private async _connectToPubnub() {
    this.log.info(`Ready my events name`);
    this.myResponseName = `${ this.cwd }`.replace(/[\W-]/g, '').toLowerCase() +
      ((await this.getPluginConfig()).uuid || OS.hostname());
    this.log.info(`Ready my events name - ${ this.myResponseName }`);

    this.log.info(`Ready internal events`);
    this.builtInEvents = new (EVENT_EMITTER as any)();
    this.log.info(`Ready internal events - COMPLETED`);

    this.log.info(`Connect to PubNub`);
    let config = await this.getPluginConfig();
    config.uuid = config.uuid || this.myResponseName;
    this.pubnubConnection = await new pubnub(config);
    this.log.info(`Connected to Pubnub`);

    await this._setupEventListener();
    //await this._setupEARHandler();
  }

  private async _emitEventAsync<T = any>(queueKey: string, plugin: string, pluginName: string | null, event: string, data?: T, additionalArgs?: any): Promise<void> {
    const self = this;
    if (Tools.isNullOrUndefined((await self.getPluginConfig()).publishKey)) {
      self.log.debug(plugin, ` - EMIT[INTERNAL]: [${ queueKey }-${ pluginName || plugin }-${ event }]`, data);
      return self.builtInEvents.emit(`${ queueKey }-${ pluginName || plugin }-${ event }`, data);
    }
    self.log.debug(plugin, ` - EMIT: [${ queueKey }-${ pluginName || plugin }-${ event }]`, data);
    /*return self.sendQu.push(async () => {
      */
    await self.pubnubConnection.publish({
      message: JSON.stringify(data),
      channel: `${ queueKey }-${ pluginName || plugin }-${ event }`,
      sendByPost: false, // true to send via post
      ...(additionalArgs || {})
    });
    self.log.debug(plugin, ` - EMIT: [${ queueKey }-${ pluginName || plugin }-${ event }] - EMITTED`, data);
    //});
  }

  async onEvent<T = any>(plugin: string, pluginName: string | null, event: string, listener: (data: T) => void): Promise<void> {
    this.log.info(plugin, ` - LISTEN: [${ this.pubnubConnectionEmitChannelKey }-${ pluginName || plugin }-${ event }]`);
    await this._subscribe<T>(`${ this.pubnubConnectionEmitChannelKey }-${ pluginName || plugin }-${ event }`, listener);
    this.log.info(plugin, ` - LISTEN: [${ `${ this.pubnubConnectionEmitChannelKey }-${ pluginName || plugin }-${ event }` }] - LISTENING`);
  }
  async emitEvent<T = any>(plugin: string, pluginName: string | null, event: string, data?: T): Promise<void> {
    await this._emitEventAsync<T>(this.pubnubConnectionEmitChannelKey, plugin, pluginName, event, data, {
      storeInHistory: true,
      ttl: this.pubnubConnectionEmitTTL
    });
  }

  private channels: Array<string> = [];
  private async _subscribe<T = any>(channel: string, listener: (data: T) => void) {
    this.builtInEvents.on(channel, listener);
    this.channels.push(channel);
    await this.pubnubConnection.subscribe({
      channels: this.channels,
    });
  }
  private async _setupEventListener() {
    const self = this;
    this.log.info(`Ready listeners`);
    this.pubnubConnection.addListener({
      message: (m: pubnub.MessageEvent) => {
        self.builtInEvents.emit(m.channel, JSON.parse(m.message));
      },
      status: (s: pubnub.StatusEvent) => {
        self.log.info(s);
      }
    });
    this.log.info(`Ready listeners - COMPLETE`);
  }
  /*private async _setupEARHandler() {
    this.log.info(`Open EAR channel (${ this.pubnubConnectionEARChannelKeyMine }-${ OS.hostname() }-${ this.myResponseName })`);
    const self = this;
    this._subscribe(`${ this.pubnubConnectionEARChannelKeyMine }-${ OS.hostname() }-${ this.myResponseName }`, (msg) => {
      self.log.debug(`[RECEVIED ${ self.myResponseName }]:`, msg);
      self.builtInEvents.emit(msg.id, msg);
    });
    this.log.info(`Open EAR channel (${ this.pubnubConnectionEARChannelKeyMine }-${ OS.hostname() }-${ this.myResponseName }) - COMPLETED`);
  }*/
  async onReturnableEvent<ArgsDataType = any, ReturnDataType = any>(callerPluginName: string, pluginName: string, event: string, listener: { (data: ArgsDataType): Promise<ReturnDataType>; }): Promise<void> {
    throw new Error('PUBNUB Events lib not setup to handle EAR!');
    /*const self = this;
    self.log.info(callerPluginName, ` - LISTEN EAR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || callerPluginName }-${ event }]`);

    self._subscribe<transEAREvent<ArgsDataType>>(`${ self.pubnubConnectionEARChannelKey }-${ pluginName || callerPluginName }-${ event }`, (bodyObj) => {
      listener((x: any) => {
        self.log.debug(callerPluginName, ` - RETURN OKAY: [${ self.pubnubConnectionEARChannelKeyMine }-${ pluginName || callerPluginName }-${ event }]`, bodyObj);
        self._emitEventAsync(self.pubnubConnectionEARChannelKeyMine, callerPluginName, bodyObj.plugin, bodyObj.topic, {
          data: x,
          id: bodyObj.id,
          resultSuccess: true
        } as internalEvent<ArgsDataType>, {
          storeInHistory: false,
          ttl: self.pubnubConnectionEARTTL
        }).then(self.log.debug).catch(self.log.fatal);
      }, (x: any) => {
        self.log.debug(callerPluginName, ` - RETURN ERROR: [${ self.pubnubConnectionEARChannelKeyMine }-${ pluginName || callerPluginName }-${ event }]`, bodyObj);
        self._emitEventAsync(self.pubnubConnectionEARChannelKeyMine, callerPluginName, bodyObj.plugin, bodyObj.topic, {
          data: x,
          id: bodyObj.id,
          resultSuccess: false
        } as internalEvent<ArgsDataType>, {
          storeInHistory: false,
          ttl: self.pubnubConnectionEARTTL
        }).then(self.log.debug).catch(self.log.fatal);
      }, bodyObj.data);
    });
    self.log.info(callerPluginName, ` - LISTEN EAR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || callerPluginName }-${ event }] - LISTENING`);*/
  }
  emitEventAndReturn<T1 = any, T2 = void>(plugin: string, pluginName: string | null, event: string, data?: T1, timeoutSeconds: number = 10): Promise<T2> {
    throw new Error('PUBNUB Events lib not setup to handle EAR!');
    /*const self = this;
    this.log.debug(plugin, ` - EMIT EAR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }-${ event }]`, data);
    return new Promise((resolve, reject) => {
      const resultKey = `${ UUID() }-${ new Date().getTime() }${ Math.random() }`;
      const xtimeoutSeconds = timeoutSeconds || 10;
      const additionalArgs: any = {
        storeInHistory: false,
        ttl: (xtimeoutSeconds * 1000) + 5000,
      };
      if (additionalArgs.ttl >= self.pubnubConnectionEARTTL) return reject(`TTL CANNOT BE GREATER THAN: ${ self.pubnubConnectionEARTTL - 2 }ms`);

      const listener = (data: internalEvent<T2>): void | any => {
        if (timeoutTimer === null)
          return this.log.debug(plugin, ` - REC EAR TOO LATE: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }-${ event }]`, data.resultSuccess ? 'SUCCESS' : 'ERRORED', data);
        this.log.debug(plugin, ` - REC EAR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }-${ event }]`, data.resultSuccess ? 'SUCCESS' : 'ERRORED', data);
        clearTimeout(timeoutTimer);
        timeoutTimer = null;
        if (data.resultSuccess)
          resolve(data.data);
        else
          reject(data.data);
      };

      let timeoutTimer: any = setTimeout(() => {
        if (timeoutTimer === null)
          return;
        clearTimeout(timeoutTimer);
        timeoutTimer = null;
        self.builtInEvents.removeListener(resultKey, listener);
        self.log.debug(plugin, ` - EMIT AR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }-${ event }-${ resultKey }]`, 'TIMED OUT');
        reject(`NO RESPONSE IN TIME: ${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }/${ resultKey } x${ ((data || {}) as any).timeoutSeconds || 10 }s`);
      }, timeoutSeconds * 1000);

      self.builtInEvents.once(resultKey, listener);

      self._emitEventAsync(self.pubnubConnectionEARChannelKey, plugin, pluginName, event, {
        id: resultKey,
        data: data,
        plugin: OS.hostname(),
        topic: self.myResponseName
      } as transEAREvent<T1>, {
        storeInHistory: false,
        ttl: self.pubnubConnectionEARTTL
      }).then(self.log.debug).catch(self.log.fatal);

      self.log.debug(plugin, ` - EMIT EAR: [${ self.pubnubConnectionEARChannelKey }-${ pluginName || plugin }-${ event }-${ resultKey }] - EMITTED`, data);
    });*/
  }
  receiveStream(callerPluginName: string, listener: (error: Error | null, stream: Readable) => Promise<void>, timeoutSeconds?: number): Promise<string> {
    throw new Error('PUBNUB Events lib not setup to handle data streams!');
  }
  sendStream(callerPluginName: string, streamId: string, stream: Readable): Promise<void> {
    throw new Error('PUBNUB Events lib not setup to handle data streams!');
  }
}